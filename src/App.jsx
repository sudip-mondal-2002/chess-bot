import './App.css';
import { Game } from 'js-chess-engine'
function App() {
  const game = new Game();
  var rows = ["8", "7", "6", "5", "4", "3", "2", "1"];
  var cols = ["a", "b", "c", "d", "e", "f", "g", "h"];
  var buttonSelected = "";
  var possibleMoves = [];
  var toMove = "";
  var position = {
    "a8": '♜', "b8": '♞', "c8": '♝', "d8": '♛',
    "e8": '♚', "f8": '♝', "g8": '♞', "h8": '♜',
    "a7": '♟', "b7": '♟', "c7": '♟', "d7": '♟',
    "e7": '♟', "f7": '♟', "g7": '♟', "h7": '♟',
    "a6": '.', "b6": '.', "c6": '.', "d6": '.',
    "e6": '.', "f6": '.', "g6": '.', "h6": '.',
    "a5": '.', "b5": '.', "c5": '.', "d5": '.',
    "e5": '.', "f5": '.', "g5": '.', "h5": '.',
    "a4": '.', "b4": '.', "c4": '.', "d4": '.',
    "e4": '.', "f4": '.', "g4": '.', "h4": '.',
    "a3": '.', "b3": '.', "c3": '.', "d3": '.',
    "e3": '.', "f3": '.', "g3": '.', "h3": '.',
    "a2": '♙', "b2": '♙', "c2": '♙', "d2": '♙',
    "e2": '♙', "f2": '♙', "g2": '♙', "h2": '♙',
    "a1": '♖', "b1": '♘', "c1": '♗', "d1": '♕',
    "e1": '♔', "f1": '♗', "g1": '♘', "h1": '♖'
  }
  const play = (from, to) => {
    from = from.toLowerCase();
    to = to.toLowerCase();
    var temp = position; 
    temp[to] = temp[from]; 
    temp[from] = '.'; 
    position = temp; 
    var fromBut = document.getElementById(from); 
    var toBut = document.getElementById(to);
    if (fromBut.innerHTML === "♔" && from==="e1" && to==="g1") { 
      play("h1","f1"); 
    }
    if (fromBut.innerHTML === "♔" && from==="e1" && to==="c1") { 
      play("a1","d1"); 
    }
    if (fromBut.innerHTML === "♚" && from==="e8" && to==="c8") { 
      play("a8","d8"); 
    }
    if (fromBut.innerHTML === "♚" && from==="e8" && to==="g8") { 
      play("h8","f8"); 
    }
    if (fromBut.innerHTML === "♙" && to[1]==='8') {
      fromBut.innerHTML = "♕";
    }
    if (fromBut.innerHTML === "♟" && to[1]==='1') {
      fromBut.innerHTML = "♛";
    }
    toBut.innerHTML = fromBut.innerHTML; 
    fromBut.innerHTML = '.';
  }
  const clickHandler = (e) => {
    if (buttonSelected === "") {
      buttonSelected = e.target.id;
      possibleMoves = game.moves(buttonSelected);
      document.getElementById(buttonSelected).classList.add("selected");
      possibleMoves.forEach(move => {
        move = move.toLowerCase();
        document.getElementById(move).classList.add("moves");
      });
    } else {
      toMove = e.target.id;
      toMove = toMove.toUpperCase();
      if (possibleMoves.indexOf(toMove) > -1) {
        game.move(buttonSelected, toMove);
        play(buttonSelected, toMove);
        if(Object.keys(game.moves()).length===0){
          alert("you won");
        };
        var compMove = game.aiMove(document.getElementById("difficulty").value);
        var compFrom = Object.keys(compMove)[0]
        play(compFrom,compMove[compFrom]);
      } else {
        // wrong move
      }
      document.getElementById(buttonSelected).classList.remove("selected");
      possibleMoves.forEach(move => {
        move = move.toLowerCase();
        document.getElementById(move).classList.remove("moves");
      });
      if(Object.keys(game.moves()).length===0){
        alert("you lost");
      };
      buttonSelected = "";
      possibleMoves = [];
    }
  }
  return (
    <div className="App">
      {rows.map((row,i) => {
        return <div key={row} className="row">
          {cols.map((col,j) => {
            var name = col + row;
            var flag = (i+j)%2 === 0 ? "#AAAAD0A0" : "#AAAAD0";
            return <button className="chessbutton" style={{backgroundColor:flag}} key={name} id={name} onClick={clickHandler}>{position[name]}</button>
          })}
        </div>
      })}
      <h4>Choose the difficulty lvl </h4>
      <h6><span>*higher difficulty will make the the bot slow</span></h6>
      <select id="difficulty" defaultValue="2">
        <option value="0">Well trained monkey</option>
        <option value="1">Beginner</option>
        <option value="2">Intermediate</option>
        <option value="3">Advanced</option>
        <option value="4">Experienced</option>
      </select>
    </div>
  );
}

export default App;
